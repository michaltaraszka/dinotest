'use strict';

// Declare app level module which depends on views, and components
lume.island.$module.config(['$locationProvider', '$routeProvider', function ($locationProvider, $routeProvider) {
    $locationProvider.hashPrefix('!');
}]);


lume.island.$module.controller('CanvasController', ['$scope', '$document', '$interval', function ($scope, $document, $interval) {

    var self = this;

    this.renderer = PIXI.autoDetectRenderer(640, 480);
    var stage = game.stage = new PIXI.Container();

    this.init = function () {
        console.log("Initializing canvas view.");
        $document[0].getElementById('canvasView').appendChild(self.renderer.view);
        game.populate();
        game.init();

        var loopStep = function () {
            self.draw();
            requestAnimationFrame(loopStep);
        };

        requestAnimationFrame(loopStep);
    };

    this.draw = function () {
        game.update(1);
        self.renderer.render(stage);
    };
}]);
