var namespace = function (path) {
  return { path: path };
};

/** @namespace */
var lume = namespace("lume");

/** @namespace */
lume.island = namespace("lume.island");

/** @namespace */
lume.island.components = namespace("lume.island.components");

/** @namespace */
lume.island.entities = namespace("lume.island.entities");

/** @namespace */
lume.island.game = namespace("lume.island.game");

lume.island.$module = angular.module('island', [
    'ngRoute',
    'island.version'
]);
