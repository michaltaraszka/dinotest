/**
 * Entity.
 *
 * @public
 * @constructor
 *
 * @param {string} entity ID.
 * @param {number} x translation.
 * @param {number} y translation.
 * @param {Array.<string>} tags array.
 */
lume.island.entities.Entity = function (id, x, y, tags) {

    var self = this;

    /**
     * Entity ID
     * @type {string}
     */
    this.id = id;

    /**
     * Tags
     * @type {Array.<string>}
     */
    this.tags = tags;


    /**
     * X translation
     * @type {number}
     */
    this.x = x;

    /**
     * Y translation
     * @type {number}
     */
    this.y = y;

    /**
     * Components
     * @type {Array}
     */
    this.components = [];

    this.addTag = function (tag) {
        self.tags.push(tag);
    };

    this.removeTag = function (tag) {
        self.tags.remove(tag);
    };

    this.addComponent = function (component) {
        self.components.push(component);
    };

    this.removeComponent = function (component) {
        var index = self.components.indexOf(component);
        if (index != -1) {
            self.components.splice(index, 1);
            component.destroy();
        }
    };

    this.getComponent = function (type) {
        return self.components.find(function (component) {
            return component instanceof type;
        });
    };

    this.init = function () {
        self.components.forEach(function (component) {
            component.init(self);
        });
    };

    this.update = function (delta) {
        self.components.forEach(function (component) {
            component.update(self, delta);
        });
    };

    this.destroy = function () {
        self.components.forEach(function (component) {
            component.destroy(self);
        });
    };
};

lume.island.entities.Entity.prototype.constructor = lume.island.entities.Entity;