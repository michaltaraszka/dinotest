lume.island.components.Dino = function (strength, speed, group, range, eyeRange, type) {

    var self = this;

    this.speed = speed;
    this.strength = strength;
    this.group = group;

    this.range = range;
    this.eyeRange = eyeRange;

    this.type = type;

    this.calories = 100;

    this.target = null;

    this.alive = true;

    var waypoint = null;

    this.children = [];
    this.parent = null;

    this.update = function (entity, delta) {
        if (self.alive) {
            if (self.type == 'carnivore') {
                if (self.target) {
                    if (getDistance(self.target) < 5) {
                        if (self.target.alive) {
                            attack(self.target);
                        } else {
                            eat(self.target);
                            if (self.target.calories < 0) {
                                self.target = null;
                            }
                        }
                    } else {
                        follow(self.target);
                    }
                } else if (self.hunger < 0.5) {
                    self.target = searchForPrey(entities);
                    if (!self.target) {
                        freeWalk(map);
                    } else {
                        waypoint = null;
                    }
                } else {
                    freeWalk(map);
                }
                self.hunger -= 0.02;
            } else if (self.type == 'herbivore') {
                if (self.target) {
                    if (self.health < 30) {
                        escape(self.target);
                    } else if (getDistance(self.target) < 5) {
                        attack(self.target);
                    } else {
                        follow(self.target);
                    }
                } else if (self.hunger < 0.5) {
                    self.target = searchForFoodSource();
                    if (!self.target) {
                        freeWalk(map);
                    } else {
                        waypoint = null;
                    }
                } else {
                    freeWalk(map);
                }
            }
            if (self.health < 0) {
                self.alive = false;
            }
        } else {
            self.calories -= 0.1;
            if (self.calories < 0) {
                game.removeEntity(self);
            }
        }
    };

    var getDistance = function (target) {
        return Math.sqrt(Math.pow(target.x - self.x, 2) + Math.pow(target.y - self.y, 2));
    };

    var eat = function (target) {
        target.calories -= 1;
        self.hunger += 1;
    };

    var getClosestHerbivore = function (entities) {
        var closestDistance = self.eyeRange;
        var closestHerbivore = null;
        entities.forEach(function (element) {
            var entity = element.component;
            if (entity instanceof Dino && entity.type === 'herbivore') {
                var distance = getDistance(entity);
                if (distance < closestDistance && distance < self.eyeRange) {
                    closestDistance = distance;
                    closestHerbivore = entity;
                }
            }
        });
        return closestHerbivore;
    };

    this.clearChildren = function () {
        self.children.forEach(function (dino) {
            dino.clearParent();
        });
    };

    this.setParent = function (dino) {
        parent.children.put(dino);
        self.parent = dino;
    };

    this.clearParent = function () {
        self.parent.children.remove(self);
        self.parent = null;
    };

    var attack = function (target) {
        target.health -= (self.strength / 2) - (Math.random() * self.strength / 2);
        if (!target.target) {
            target.target = self;
        }
    };

    var freeWalk = function (map) {
        if (waypoint) {
            follow(waypoint);
            if (getDistance(waypoint) < self.speed) {
                waypoint = null;
            }
        } else {
            waypoint = {
                x: Math.floor(Math.random() * map.width),
                y: Math.floor(Math.random() * map.height)
            }
        }
    };

    var follow = function (target) {
        var xTranslation = target.x - self.x;
        var yTranslation = target.y - self.y;
        var length = Math.sqrt(Math.pow(xTranslation, 2) + Math.pow(yTranslation, 2));
        xTranslation /= length;
        yTranslation /= length;
        self.x += xTranslation * self.speed;
        self.y += yTranslation * self.speed;
    };

    var findClosestGroupMember =  function () {
        var leader = getLeader(self);
        var members = [];
        leader.getAllChildren(members);
        var closestMember = null;
        var closestDistance = 0;
        members.forEach(function (member) {
            var distance = getDistance(member);
            if (closestDistance > distance) {
                closestDistance = distance;
                closestMember = member;
            }
        });
        self.setParent();
    };

    this.getAllChildren = function (members) {
        this.children.forEach(function (child) {
            members.put(self);
            child.getAllChildren(members);
        });
    };

    var getLeader = function () {
        if (self.parent) {
            return getLeader(self.parent);
        } else {
            return self;
        }
    };

    this.followParent = function () {
        if (getDistance(self.parent) > 20) {
            follow(self.parent);
        } else {

        }
    };

    var escape = function (target) {
        var xTranslation = target.x - self.x;
        var yTranslation = target.y - self.y;
        var length = Math.sqrt(Math.pow(xTranslation, 2) + Math.pow(yTranslation, 2));
        xTranslation /= length;
        yTranslation /= length;
        xTranslation *= -1;
        yTranslation *= -1;
        self.x += xTranslation * self.speed;
        self.y += yTranslation * self.speed;
    };

    var searchForPrey = function (entities) {
        return getClosestHerbivore(entities);
    };

    var searchForFoodSource = function () {

    };

};