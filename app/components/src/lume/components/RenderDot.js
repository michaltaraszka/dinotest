lume.island.components.RenderDot = function (size) {

    var self = this;

    this.size = size;

    this.color = 0x000000;

    this.renderable = new PIXI.Graphics;

    this.init = function (entity) {
        self.renderable.beginFill(self.color);
        self.renderable.drawCircle(0, 0, self.size);
        game.stage.addChild(self.renderable);
    };

    this.update = function (entity) {
        var gameEntity = game.entitiesModel.getEntityByID("game");
        var cameraPosition = gameEntity.getComponent(lume.island.components.MoveMap).cameraPosition;
        self.renderable.x = entity.x + cameraPosition.x;
        self.renderable.y = entity.y + cameraPosition.y;
        self.renderable.radius = entity.size;
    };

    this.destroy = function (entity) {
        game.stage.removeChild(self.renderable);
    };

};

lume.island.components.RenderDot.prototype.constructor = lume.island.components.RenderDot;