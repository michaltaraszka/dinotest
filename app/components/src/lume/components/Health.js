lume.island.components.Health = function (calories) {

    var self = this;

    this.health = 0;
    this.calories = calories;

    this.alive = true;
    this.bones = false;

    var attributes = null;

    this.init = function (entity) {
        attributes = entity.getComponent(lume.island.components.Attributes);
    };

    this.update = function (entity, delta) {

        if (!self.calories) {
            if (self.hunger - delta > 0) {
                self.hunger -= delta;
            } else {
                self.hunger = 0;
                self.starving = true;
            }
        }
    };

    this.destroy = function (entity) {

    };

    this.dealDamage = function (attack) {
        var damage = attack / attributes.defense;
        if (self.health - damage > 0) {
            self.health -= damage;
        } else {
            self.health = 0;
            self.alive = false;
        }
    };

    this.eat = function (amount) {
        if (!self.bones) {
            if (self.calories - amount > 0) {
                self.calories -= amount;
                return amount;
            } else {
                self.bones = true;
                return self.calories;
            }
        } else {
            return 0;
        }
    };

};

lume.island.components.Health.prototype.constructor = lume.island.components.Health;
