lume.island.components.CarnivoreBehavior = function () {

    var self = this;

    this.target = null;

    var waypoint = null;

    var health = null;
    var hunger = null;
    var attributes = null;

    var parent;

    this.init = function (entity) {
        parent = entity;
        health = entity.getComponent(lume.island.components.Health);
        hunger = entity.getComponent(lume.island.components.Hunger);
        attributes = parent.getComponent(lume.island.components.Attributes);
    };

    this.update = function (entity, delta) {
        if (health.alive) {
            if (self.target) {
                if (getDistance(parent, self.target) < 5) {
                    if (self.target.getComponent(lume.island.components.Health).alive) {
                        attack(parent, self.target);
                    } else {
                        eat(self.target);
                        if (self.target.getComponent(lume.island.components.Health).bones) {
                            self.target = null;
                        }
                    }
                } else {
                    follow(parent, self.target);
                }
            } else if (hunger.hunger < 0.5) {
                self.target = searchForPrey(entity);
                if (!self.target) {
                    freeWalk();
                } else {
                    waypoint = null;
                }
            } else {
                freeWalk();
            }
        }
    };

    this.destroy = function (entity) {

    };

    var getDistance = function (entity, target) {
        return Math.sqrt(Math.pow(target.x - entity.x, 2) + Math.pow(target.y - entity.y, 2));
    };

    var eat = function (target) {
        target.getComponent(lume.island.components.Hunger).eat(1);
    };

    var getClosestHerbivore = function () {
        var closestDistance = attributes.eyeRange;
        var closestHerbivore = null;
        game.entitiesModel.getEntitiesByTag("herbivore").forEach(function (herbivore) {
            var distance = getDistance(parent, herbivore);
            if (distance < closestDistance && distance < attributes.eyeRange) {
                closestDistance = distance;
                closestHerbivore = herbivore;
            }
        });
        return closestHerbivore;
    };

    var attack = function (entity, target) {
        health.health -= (attributes.strength / 2) - (Math.random() * attributes.strength / 2);
        if (!target.target) {
            target.target = entity;
        }
    };

    var freeWalk = function () {
        if (waypoint) {
            follow(parent, waypoint);
            if (getDistance(parent, waypoint) < attributes.speed) {
                waypoint = null;
            }
        } else {
            waypoint = {
                x: Math.floor(Math.random() * game.map.width),
                y: Math.floor(Math.random() * game.map.height)
            }
        }
    };

    var follow = function (entity, target) {
        var xTranslation = target.x - entity.x;
        var yTranslation = target.y - entity.y;
        var length = Math.sqrt(Math.pow(xTranslation, 2) + Math.pow(yTranslation, 2));
        xTranslation /= length;
        yTranslation /= length;
        entity.x += xTranslation * attributes.speed;
        entity.y += yTranslation * attributes.speed;
    };


    var escape = function (entity, target) {
        var xTranslation = target.x - entity.x;
        var yTranslation = target.y - entity.y;
        var length = Math.sqrt(Math.pow(xTranslation, 2) + Math.pow(yTranslation, 2));
        xTranslation /= length;
        yTranslation /= length;
        xTranslation *= -1;
        yTranslation *= -1;
        entity.x += xTranslation * attributes.speed;
        entity.y += yTranslation * attributes.speed;
    };

    var searchForPrey = function () {
        return getClosestHerbivore();
    };

};

lume.island.components.CarnivoreBehavior.prototype.constructor = lume.island.components.CarnivoreBehavior;