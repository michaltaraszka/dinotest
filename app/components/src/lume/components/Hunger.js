lume.island.components.Hunger = function () {

    var self = this;

    var maxHunger = 100;

    this.hunger = 0;

    this.starving = false;
    
    this.init = function () {
        
    };

    this.update = function (entity, delta) {
        if (!self.starving) {
            if (self.hunger - delta > 0) {
                self.hunger -= delta;
            } else {
                self.hunger = 0;
                self.starving = true;
            }
        }
    };

    this.destroy = function (entity) {

    };

    this.eat = function (calories) {
        if (calories > 0) {
            if (this.hunger + calories > maxHunger) {
                this.hunger = maxHunger;
            } else {
                this.hunger += calories;
            }
            this.starving = false;
        }
    };

};

lume.island.components.Hunger.prototype.constructor = lume.island.components.Hunger;
