lume.island.components.MoveMap = function () {

    var self = this;

    var fadeTimeInMilliseconds = 4000;
    var defaultSpeed = 5;

    var currentTime = 0;
    var lastTime =  0;

    this.cameraPosition = {
        x: 0,
        y: 0,
        move: function (vector) {
            self.cameraPosition.x += vector.x;
            self.cameraPosition.y += vector.y;
        }
    };

    this.init = function (entity) {
        keyboardJS.bind('s', function () {
            y.direction = -1;
            y.resetTime = true;
        });
        keyboardJS.bind('w', function () {
            y.direction = 1;
            y.resetTime = true;
        });
        keyboardJS.bind('a', function () {
            x.direction = 1;
            x.resetTime = true;
        });
        keyboardJS.bind('d', function () {
            x.direction = -1;
            x.resetTime = true;
        });
    };

    var x = {
        currentTime: 0,
        resetTime: false,
        direction: 0
    };

    var y = {
        currentTime: 0,
        resetTime: false,
        direction: 0
    };

    this.update = function (entity, delta) {
        currentTime = Date.now();

        console.log("x: " + Math.floor(self.cameraPosition.x) + ",y: " + Math.floor(self.cameraPosition.y));

        self.cameraPosition.move({
            x: calculateFinalSpeed(x, delta),
            y: calculateFinalSpeed(y, delta)
        });

        lastTime = currentTime;
    };

    var calculateFinalSpeed = function (axis, delta) {
        if (axis.resetTime) {
            axis.currentTime = 0;
            axis.resetTime = false;
            return defaultSpeed * delta * axis.direction;
        }  if (axis.currentTime <= fadeTimeInMilliseconds) {
            axis.currentTime += currentTime - lastTime;
            return interpolate(defaultSpeed * delta, axis.direction, axis.currentTime)
        }
        return 0;
    };

    var interpolate = function (value, direction, currentTime) {
        return value * direction * ((fadeTimeInMilliseconds - currentTime) / fadeTimeInMilliseconds);
    };

    this.destroy = function (entity) {
    };
};

lume.island.components.MoveMap.prototype.constructor = lume.island.components.MoveMap;
