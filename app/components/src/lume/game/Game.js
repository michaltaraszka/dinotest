lume.island.game.Game = function () {
    var self = this;

    this.entitiesModel = new lume.island.game.EntitiesModel();

    this.stage = null;

    this.map = {
        width: 5000,
        height: 5000
    };

    this.populate = function () {
        var gameLogicEntity = createGameLogicEntity();
        self.entitiesModel.entities.push(gameLogicEntity);

        var i;
        var entity;

        for (i = 0; i < Math.floor((Math.random() * 10)) + 500; i++) {
            entity = createEntity("carnivore_" + i, "carnivore");
            self.entitiesModel.addEntity(entity);
        }
        for (i = 0; i < Math.floor((Math.random() * 10)) + 500; i++) {
            entity = createEntity("herbivore_" + i, "herbivore");
            self.entitiesModel.addEntity(entity);
        }
    };

    this.init = function () {
        self.entitiesModel.entities.forEach(function (entity) {
            entity.init(entity);
        });
    };

    var createEntity = function (id, type) {
        var x = Math.floor((Math.random() * self.map.width));
        var y = Math.floor((Math.random() * self.map.height));
        var entity = new lume.island.entities.Entity(id, x, y, ["dinosaur", "unit", type]);

        entity.addComponent(new lume.island.components.Hunger());

        entity.addComponent(new lume.island.components.Attributes(10, 100, 2, 5, 10));

        var renderable = new lume.island.components.RenderDot(5);
        entity.addComponent(renderable);

        if (type == 'carnivore') {
            renderable.color = 0xFF0000;
            entity.addComponent(new lume.island.components.Health(50));
            entity.addComponent(new lume.island.components.CarnivoreBehavior());
        } else {
            renderable.color = 0x0000FF;
            entity.addComponent(new lume.island.components.Health(200));
        }

        return entity;
    };

    var createGameLogicEntity = function () {
        var entity = new lume.island.entities.Entity("game", 0, 0, ["logic"]);
        entity.addComponent(new lume.island.components.MoveMap());
        return entity;
    };

    this.update = function (delta) {
        self.entitiesModel.entities.forEach(function (entity) {
            entity.update(delta);
        });
        self.entitiesModel.refresh();
    };

};

lume.island.game.Game.prototype.constructor = lume.island.game.Game;

var game = new lume.island.game.Game();