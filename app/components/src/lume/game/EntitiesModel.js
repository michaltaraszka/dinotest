lume.island.game.EntitiesModel = function () {

    var self = this;

    this.entities = [];

    this.entitiesToRemove = [];
    this.entitiesToAdd = [];

    this.removeEntity = function (entity) {
        self.entitiesToRemove.push(entity);
    };

    this.addEntity = function (entity) {
        self.entitiesToAdd.push(entity);
    };

    this.getEntityByID = function (id) {
        return self.entities.find(function (entity) {
            return entity.id === id;
        });
    };

    this.getEntitiesByTag = function (tag) {
        return self.entities.filter(function (entity) {
            return entity.tags.indexOf(tag) != -1;
        });
    };

    this.refresh = function () {
        self.entitiesToRemove.forEach(function (entity) {
            var index = self.entities.indexOf(entity);
            if (index > -1) {
                self.entities.splice(index, 1);
            }
            entity.destroy();
        });
        self.entitiesToRemove = [];

        self.entitiesToAdd.forEach(function (entity) {
            self.entities.push(entity);
            entity.init(entity);
        });
        self.entitiesToAdd = [];
    };
};

lume.island.game.EntitiesModel.prototype.constructor = lume.island.game.EntitiesModel;