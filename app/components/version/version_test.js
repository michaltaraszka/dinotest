'use strict';

describe('island.version module', function() {
  beforeEach(module('island.version'));

  describe('version service', function() {
    it('should return current version', inject(function(version) {
      expect(version).toEqual('0.1');
    }));
  });
});
