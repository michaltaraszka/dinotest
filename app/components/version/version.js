'use strict';

angular.module('island.version', [
  'island.version.interpolate-filter',
  'island.version.version-directive'
])

.value('version', '0.1');
